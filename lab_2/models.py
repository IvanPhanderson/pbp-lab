from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    from_who =  models.CharField(max_length=30) # from is a reserved word
    title =  models.CharField(max_length=30)
    message =  models.CharField(max_length=500)