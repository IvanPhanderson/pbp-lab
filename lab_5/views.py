from lab_2.models import Note
from django.shortcuts import (get_object_or_404,
                              render,
                              HttpResponseRedirect)
from lab_4.forms import NoteForm
from django.views.generic import View
from django.http import JsonResponse


class UpdateCrudUser(View):
    def  get(self, request):
        name1 = request.GET.get('to', None)
        address1 = request.GET.get('from_who', None)
        age1 = request.GET.get('title', None)
        message1 = request.GET.get('message', None)

        obj = NoteForm.objects.create(
            name = name1,
            address = address1,
            age = age1,
            message = message1
        )

        user = {'id':obj.id,'name':obj.name,'address':obj.address,'age':obj.age, 'message':obj.message}

        data = {
            'user': user
        }
        return JsonResponse(data)

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # add the dictionary during initialization
    context["note"] = Note.objects.get(id = id)
         
    return render(request, "detail_view.html", context)

# update view for details
def update_view(request, id):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Note, id = id)
 
    # pass the object as instance in form
    form = NoteForm(request.POST or None, instance = obj)
 
    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-5/"+id)
 
    # add form dictionary to context
    context["form"] = form
    context["note"] = Note.objects.get(id = id)
 
    return render(request, "update_view.html", context)

# delete view for details
def delete_view(request, id):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Note, id = id)
 
 
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return HttpResponseRedirect("/lab-5")
 
    return render(request, "delete_view.html", context)