from django.urls import path
from .views import index, get_note, update_view, delete_view, UpdateCrudUser

urlpatterns = [
    path('', index, name='index_lab4'),
    path('<id>', get_note),
    path('<id>/update', update_view, name="update_data"),
    path('<id>/delete', delete_view, name="delete_data"),
    path('ajax/crud/create/',  UpdateCrudUser.as_view(), name='crud_ajax_update'),
]