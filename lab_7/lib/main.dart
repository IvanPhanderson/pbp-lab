import 'package:flutter/material.dart';

import './dummy_data2.dart';
import './screens/tabs_screen.dart';
import 'screens/company_invest_screen.dart';
import 'screens/category_company_screen.dart';
import './screens/filters_screen.dart';
import './screens/categories_screen.dart';
import 'models/company.dart';
import './screens/graph_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'profit': false,
    'rugi': false,
  };
  List<Company> _availableCompany = DUMMY_COMPANY;
  List<Company> _favoriteCompany = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableCompany = DUMMY_COMPANY.where((company) {
        if (_filters['profit'] && !company.isProfit) {
          return false;
        }
        if (_filters['rugi'] && !company.isRugi) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String companyId) {
    final existingIndex =
        _favoriteCompany.indexWhere((company) => company.id == companyId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteCompany.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteCompany.add(
          DUMMY_COMPANY.firstWhere((company) => company.id == companyId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteCompany.any((company) => company.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mulai Invest',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
        canvasColor: Color.fromRGBO(205, 245, 255, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(_favoriteCompany),
        CategoryCompanyScreen.routeName: (ctx) =>
            CategoryCompanyScreen(_availableCompany),
        CompanyInvestScreen.routeName: (ctx) => CompanyInvestScreen(_toggleFavorite, _isMealFavorite),
        FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
        GraphScreen.routeName: (ctx) => GraphScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => CategoryCompanyScreen(_availableCompany),
        );
      },
    );
  }
}
