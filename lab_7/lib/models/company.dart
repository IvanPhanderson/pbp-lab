import 'package:flutter/foundation.dart';

enum Complexity {
  Simple,
  Challenging,
  Hard,
}

enum Affordability {
  MiddleHigh,
  LowBudgetFriendly,
  HighRisk,
}

class Company {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final String deskripsi;
  final String ingredients;
  final int duration;
  final Complexity complexity;
  final Affordability affordability;
  final int hargaSaham;
  final int jumlahLembar;
  final int sisaLembar;
  final bool isProfit;
  final bool isRugi;

  const Company({
    @required this.id,
    @required this.categories,
    @required this.title,
    @required this.imageUrl,
    @required this.ingredients,
    @required this.deskripsi,
    @required this.duration,
    @required this.complexity,
    @required this.affordability,
    @required this.hargaSaham,
    @required this.jumlahLembar,
    @required this.sisaLembar,
    @required this.isProfit,
    @required this.isRugi
  });
}
