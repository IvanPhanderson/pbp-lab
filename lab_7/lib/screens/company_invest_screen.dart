import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/graph_screen.dart';

import '../dummy_data2.dart';
import '../screens/graph_screen.dart';

class CompanyInvestScreen extends StatefulWidget {
  static const routeName = '/company-detail';
  // final List<Company> favoriteCompany;

  // TabsScreen(this.favoriteCompany);
  final Function toggleFavorite;
  final Function isFavorite;
  CompanyInvestScreen(this.toggleFavorite, this.isFavorite);

  @override
  _CompanyInvestScreenState createState() => _CompanyInvestScreenState();
}

// class _TabsScreenState extends State<TabsScreen> {

class _CompanyInvestScreenState extends State<CompanyInvestScreen> {
  String result = '';
  final myController = TextEditingController();

  
  final _formKey = GlobalKey<FormState>();  

  bool isInteger(String value) {
    if(value == null) {
      return false;
    }
    return int.tryParse(value) != null;
  }

  bool isPositive(String value){
    int temp = int.parse(value);
    if(temp > 0){
      return true;
    }
    return false;
  }

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final companyId = ModalRoute.of(context).settings.arguments as String;
    final selectedCompany = DUMMY_COMPANY.firstWhere((company) => company.id == companyId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedCompany.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 400,
              width: double.infinity,
              child: new Image.network(
                selectedCompany.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 532,
              // child: new Image.network(
              //   selectedCompany.graphUrl,
              //   fit: BoxFit.cover,
              // ),
              child: GraphScreen(),
            ),
             Container(
              width: 350,
                child: Text("NS = Net Sales\nGM = Gross Margin\nEBITDA = Earnings Before Interest, Taxes, Depreciation and Amortization\nSeluruhnya dalam satuan juta"),
            ),
            buildSectionTitle(context, 'Informasi Saham'),
            Container(
              width: 350,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.6),
              ),
              child: Padding(
                padding: EdgeInsets.all(25.0),
                child: Text("Jumlah saham terjual: " + (selectedCompany.jumlahLembar-selectedCompany.sisaLembar).toString() + " lembar\n" + 
                            "Jumlah saham tersisa: " + selectedCompany.sisaLembar.toString() + " lembar\n" +
                            "Harga saham: " + selectedCompany.hargaSaham.toString()),
              ),
            ),
            buildSectionTitle(context, 'Deskripsi Perusahaan'),
            Container(
              width: 350,
              // color: Colors.white,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.6),
              ),
              child: Padding(
                padding: EdgeInsets.all(25.0),
                child: Text(selectedCompany.deskripsi),
              ),
            ),
            buildSectionTitle(context, 'Investasi Anda'),
            // buildContainer(
            Container(
              width: 350,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(93, 223, 255, 1),
              ),
              child: Padding(
                  padding: EdgeInsets.all(25.0),
                  child: 
                    Text(selectedCompany.ingredients,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25)
                  )
              ),
            ),
            buildSectionTitle(context, 'Ayo Investasi!'),
            Form(  
              key: _formKey,  
              child: Container(
                width: 350,
                decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.6),
                ),
                child: Column(  
                  crossAxisAlignment: CrossAxisAlignment.start,  
                  children: <Widget>[  
                    TextFormField(  
                      controller: myController,
                      decoration: new InputDecoration(
                        hintText: "e.g. 20",
                        labelText: "Jumlah lembar saham yang ingin ditanamkan:",
                        prefixIcon: Icon(Icons.attach_money),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Input tidak boleh kosong';
                        } else if (!isInteger(value)){
                          return 'Input harus berupa bilangan bulat';
                        } else if (!isPositive(value)){
                          return 'Input harus berupa bilangan positif';
                        }
                        return null;
                      } 
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child :
                      Text(result, style: TextStyle(fontSize: 14))
                    ),
                    Row(  
                      children: <Widget>[
                        Expanded(
                          // padding: const EdgeInsets.all(25.0),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  result='Anda berhasil membeli '+myController.text + ' lembar saham';
                                });
                              } else{
                                setState(() {
                                  result='';
                                });
                              }
                            },
                            child: Text('Beli'),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12), // <-- Radius
                              ),
                            ),
                          )
                        ),
                        Expanded(
                          // padding: const EdgeInsets.all(25.0),
                          child: ElevatedButton(
                            onPressed: () {
                              // if (_formKey.currentState.validate()) {
                                Navigator.pop(context);
                              // }
                            },
                            child: Text('Batal'),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12), // <-- Radius
                              ),
                            ),
                          )
                        )
                      ]
                    ),  
                  ]
                )
              )
            ),  
          ],
        ),
      ),
      
      floatingActionButton: FloatingActionButton(
        child: Icon(
           widget.isFavorite(companyId) ? Icons.star : Icons.star_border,
        ),
        onPressed: () => widget.toggleFavorite(companyId),
      ),
    );
  }
}
