import 'package:flutter/material.dart';

import '../models/company.dart';
import '../widgets/company_item.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Company> favoriteCompany;

  FavoritesScreen(this.favoriteCompany);

  @override
  Widget build(BuildContext context) {
    if (favoriteCompany.isEmpty) {
      return Center(
        child: Text('Belum ada yang dipilih.'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favoriteCompany[index].id,
            title: favoriteCompany[index].title,
            imageUrl: favoriteCompany[index].imageUrl,
            duration: favoriteCompany[index].duration,
            affordability: favoriteCompany[index].affordability,
            complexity: favoriteCompany[index].complexity,
          );
        },
        itemCount: favoriteCompany.length,
      );
    }
  }
}
