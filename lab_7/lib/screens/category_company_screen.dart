import 'package:flutter/material.dart';

import '../widgets/company_item.dart';
import '../models/company.dart';

class CategoryCompanyScreen extends StatefulWidget {
  static const routeName = '/category-companys';

  final List<Company> availableCompany;

  CategoryCompanyScreen(this.availableCompany);

  @override
  _CategoryCompanyScreenState createState() => _CategoryCompanyScreenState();
}

class _CategoryCompanyScreenState extends State<CategoryCompanyScreen> {
  String categoryTitle;
  List<Company> displayedCompany;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      // final routeArgs =
      //     ModalRoute.of(context).settings.arguments as Map<String, String>;
      // categoryTitle = 'Abc Testes';
      // final categoryId = routeArgs['id'];
      final categoryId = 'c6';
      displayedCompany = widget.availableCompany.where((company) {
        return company.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String companyId) {
    setState(() {
      displayedCompany.removeWhere((company) => company.id == companyId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
        // title: Text(categoryTitle),
      // ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: displayedCompany[index].id,
            title: displayedCompany[index].title,
            imageUrl: displayedCompany[index].imageUrl,
            duration: displayedCompany[index].duration,
            affordability: displayedCompany[index].affordability,
            complexity: displayedCompany[index].complexity,
          );
        },
        itemCount: displayedCompany.length,
      ),
    );
  }
}
