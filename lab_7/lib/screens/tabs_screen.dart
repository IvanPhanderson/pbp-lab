import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import './favorites_screen.dart';
import './categories_screen.dart';
import '../models/company.dart';
import '../screens/category_company_screen.dart';
import '../dummy_data2.dart';

class TabsScreen extends StatefulWidget {
  final List<Company> favoriteCompany;

  TabsScreen(this.favoriteCompany);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;
  List<Company> _availableCompany = DUMMY_COMPANY;

  @override
  void initState() {
    _pages = [
      {
        'page': CategoryCompanyScreen(_availableCompany),
        'title': 'Status Investasi',
      },
      {
        'page': FavoritesScreen(widget.favoriteCompany),
        'title': 'Kesukaan Saya',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).accentColor,
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.work),
            title: Text('Status Investasi'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.star),
            title: Text('Kesukaan Saya'),
          ),
        ],
      ),
    );
  }
}
