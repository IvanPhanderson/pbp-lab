from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:  
        model = Note
        fields = '__all__'

        error_messages = {'message' : {'invalid': "Mohon mengisi DOB dengan format yyyy-mm-dd"}}

