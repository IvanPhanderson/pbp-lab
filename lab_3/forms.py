from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:  
        model = Friend
        fields = '__all__'

        error_messages = {'DOB' : {'invalid': "Mohon mengisi DOB dengan format yyyy-mm-dd"}}