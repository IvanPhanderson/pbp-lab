from django.urls import path
from .views import index, add_friend
#  xml, json

urlpatterns = [
    path('', index, name='index_lab3'),
    path('add/', add_friend)
]
