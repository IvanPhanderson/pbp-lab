### Apakah perbedaan antara JSON dan XML?
   JSON merupakan singkatan dari JavaScript Object Notation. JSON biasanya digunakan untuk menyimpan dan/atau mentransfer suatu data. Format penulisannya sendiri mengikuti format penulisan JavaScript Object yang mirip juga dengan penulisan dictionary di Python. <br>
   XML, singkatan dari eXtensible Markup Language, merupakan bahasa markup yang mirip dengan HTML dan didesain untuk menyimpan dan mentransfer data. Jika dibandingkan dengan JSON, XML sedikit lebih sulit untuk dibaca oleh manusia. <br>
   Berikut ini adalah perbedaan dari JSON dan XML:

   | JSON        | XML         |
   | ----------- | ----------- |
   | Mendukung konsep array | Tidak mendukung array |
   | Hanya mendukung tipe data teks dan angka | Mendukung banyak tipe data seperti angka, teks, grafik, dan gambar |
   | Tidak dapat menulis comment | Dapat menulis comment |
   | Tidak memerlukan tag penutup | Menggunakan tag pembuka dan penutup |
   | Sintaksnya lebih sederhana dan mudah dibaca | Sintaksnya lebih rumit untuk dibaca |
   | Lebih cepat karena datanya sudah dapat diakses sebagai JSON Objects | Lebih lambat karena perlu parsing dan parsing membutuhkan waktu yang cukup lama |

### Apakah perbedaan antara HTML dan XML?
   HTML merupakan singkatan dari Hypertext Markup Language dan merupakan bahasa standard yang digunakan untuk ketika membuat halaman web. Untuk format penulisannya, HTML terdiri atas sederet elemen yang memiliki kegunaan yang bervariasi. <br>
   XML, singkatan dari eXtensible Markup Language, merupakan bahasa markup yang mirip dengan HTML dan didesain untuk menyimpan dan mentransfer data. Walaupun mirip dengan HTML, ternyada ada banyak juga perbedaan antara XML dengan HTML, <br>
   Berikut ini adalah perbedaan antara HTML dan XML:
   | HTML        | XML         |
   | ----------- | ----------- |
   | Tidak case sensitive | Case sensitive |
   | Umumnya digunakan untuk menampilkan data | Umumnya digunakan untuk mentransfer atau menyimpan data |
   | Tidak selalu memerlukan tag penutup seperti `<br>` yang <br> tidak memerlukan tag penutup | Selalu memerlukan tag penutup |
   | Kesalahan kecil dapat diabaikan | Tidak memperbolehkan adanya error |
   | Whitespace hanya akan dianggap sebagai 1 karakter kosong | Whitespace sangat berpengaruh |
   | HTML memiliki tag standarnya sendiri | Kita dapat mendefinisikan tag sesuai kebutuhan kita |

<br>

#### Referensi
https://www.w3schools.com/xml/xml_whatis.asp <br>
https://www.geeksforgeeks.org/difference-between-json-and-xml/ <br>
https://www.guru99.com/json-vs-xml-difference.html <br>
https://www.w3schools.com/js/js_json_intro.asp <br>
https://www.w3schools.com/html/html_intro.asp <br>
https://www.geeksforgeeks.org/html-vs-xml/ <br>
https://www.javatpoint.com/html-vs-xml <br>
https://www.guru99.com/xml-vs-html-difference.html