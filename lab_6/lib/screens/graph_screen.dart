
import 'package:draw_graph/draw_graph.dart';
import 'package:draw_graph/models/feature.dart';
import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';


class GraphScreen extends StatefulWidget {
  static const routeName = '/graphs';
  GraphScreen();
  @override
  _GraphScreenState createState() => _GraphScreenState();
}

class _GraphScreenState extends State<GraphScreen> {
  final List<Feature> features = [
    Feature(
      title: "Net Sales",
      color: Colors.blue,
      data: [0.3, 0.6, 0.8, 0.9, 1, 1.2],
    ),
    Feature(
      title: "Gross Margin",
      color: Colors.black,
      data: [1, 0.8, 0.6, 0.7, 0.3, 0.1],
    ),
    Feature(
      title: "EBITDA",
      color: Colors.orange,
      data: [0.4, 0.2, 0.9, 0.5, 0.6, 0.4],
    ),
    // Feature(
    //   title: "React Native",
    //   color: Colors.red,
    //   data: [0.5, 0.2, 0, 0.3, 1, 1.3],
    // ),
    // Feature(
    //   title: "Swift",
    //   color: Colors.green,
    //   data: [0.25, 0.6, 1, 0.5, 0.8, 1,4],
    // ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white54,
      // appBar: AppBar(
      //   title: Text("Flutter Draw Graph Demo"),
        
      // ),
      // drawer: MainDrawer(),
      body: SingleChildScrollView(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        // crossAxisAlignment: CrossAxisAlignment.center,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 0.0),
              child: Text(
                "Grafik Keuangan",
                style: TextStyle(
                  fontSize: 28,
                  fontFamily: 'RobotoCondensed',
                  fontWeight: FontWeight.bold,
                  // letterSpacing: 2,
                ),
              ),
            ),
            Center(
              child: Container(
                // child: Align(
                  // alignment: Alignment.center,
                  child: (
                    LineGraph(
                      features: features,
                      size: Size(420, 450),
                      labelX: ['Day 1', 'Day 2', 'Day 3', 'Day 4', 'Day 5', 'Day 6'],
                      labelY: ['25%', '45%', '65%', '75%', '85%', '100%'],
                      showDescription: true,
                      graphColor: Colors.black87, 
                    )
                  )
                // ),
              )
            ),
            SizedBox(
              height: 50,
            )
          ],
        )
      ),
    );
  }
}