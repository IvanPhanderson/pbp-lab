import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/screens/graph_screen.dart';

import '../dummy_data2.dart';
import '../screens/graph_screen.dart';

class MealDetailScreen extends StatelessWidget {
  static const routeName = '/meal-detail';

  final Function toggleFavorite;
  final Function isFavorite;
  final _formKey = GlobalKey<FormState>();  


  MealDetailScreen(this.toggleFavorite, this.isFavorite);

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final mealId = ModalRoute.of(context).settings.arguments as String;
    final selectedCompany = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedCompany.title}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: new Image.network(
                selectedCompany.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 560,
              // width: double.infinity,
              // child: new Image.network(
              //   selectedCompany.graphUrl,
              //   fit: BoxFit.cover,
              // ),
              child: GraphScreen(),
            ),
            buildSectionTitle(context, 'Informasi Saham'),
            Container(
              width: 300,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.7),
              ),
              child: Padding(
                padding: EdgeInsets.all(25.0),
                child: Text("Jumlah saham terjual: " + (selectedCompany.jumlahLembar-selectedCompany.sisaLembar).toString() + " lembar\n" + 
                            "Jumlah saham tersisa: " + selectedCompany.sisaLembar.toString() + " lembar\n" +
                            "Harga saham: " + selectedCompany.hargaSaham.toString()),
              ),
            ),
            buildSectionTitle(context, 'Deskripsi Perusahaan'),
            Container(
              width: 300,
              // color: Colors.white,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.7),
              ),
              child: Padding(
                padding: EdgeInsets.all(25.0),
                child: Text(selectedCompany.deskripsi),
              ),
            ),
            buildSectionTitle(context, 'Investasi Anda'),
            // buildContainer(
            Container(
              width: 300,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.7),
              ),
              child: Padding(
                  padding: EdgeInsets.all(25.0),
                  child: 
                    Text(selectedCompany.ingredients,
                    textAlign: TextAlign.center,
                  )
              ),
            ),
            buildSectionTitle(context, 'Ayo Investasi!'),
            Form(  
              key: _formKey,  
              child: Container(
                width: 300,
                decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(16.0),
                color: Color.fromRGBO(255, 255, 255, 0.7),
                ),
                child: Column(  
                  crossAxisAlignment: CrossAxisAlignment.start,  
                  children: <Widget>[  
                    TextFormField(  
                      decoration: const InputDecoration(  
                        icon: const Icon(Icons.attach_money),  
                        hintText: 'e.g. 20',  
                        labelText: 'Jumlah lembar saham yang ingin ditanamkan:',  
                      ),  
                    ),
                    Row(  
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(25.0),
                          child: new RaisedButton(  
                            color: Colors.blue,
                            child: const Text('Submit'),  
                            onPressed: null,  
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(25.0),
                          child: new RaisedButton(  
                            color: Colors.red,
                            child: const Text('Batal'),  
                            onPressed: null,  
                          ),
                        )
                      ]
                    ),  
                  ]
                )
              )
            ),  
          ],
        ),
      ),
      
      floatingActionButton: FloatingActionButton(
        child: Icon(
           isFavorite(mealId) ? Icons.star : Icons.star_border,
        ),
        onPressed: () => toggleFavorite(mealId),
      ),
    );
  }
}
