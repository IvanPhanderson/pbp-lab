import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c4',
    title: 'Ternak Babi Ngepet',
    color: Colors.amber,
  ),
  Category(
    id: 'c5',
    title: 'Pebepe\'s Shop',
    color: Colors.blue,
  ),
  Category(
    id: 'c6',
    title: 'BizzVest',
    color: Colors.green,
  ),
];

const DUMMY_MEALS = const [
  // Meal(
  //   id: 'm1',
  //   categories: [
  //     'c1',
  //   ],
  //   title: 'Toko Jam Abadi',
  //   affordability: Affordability.MiddleHigh,
  //   complexity: Complexity.Simple,
  //   imageUrl:
  //       'https://tangselmedia.com/wp-content/uploads/2020/01/6-Daftar-Toko-Jam-Terbaik-di-Tangerang-Selatan-aslie.png',
  //   duration: 20,
  //   ingredients: [
  //     'Current profit: (+) Rp20.000.000',
  //     'Good Investment 👍'
  //   ],
  //   hargaSaham: 20000,
  //   jumlahLembar: 100,
  //   sisaLembar: 20,
  //   isProfit: true,
  //   isRugi: false,
  // ),
  // Meal(
  //   id: 'm2',
  //   categories: [
  //     'c2',
  //   ],
  //   title: 'Murah Meriah',
  //   affordability: Affordability.MiddleHigh,
  //   complexity: Complexity.Simple,
  //   imageUrl:
  //       'https://bisnisukm.com/uploads/2010/03/serbu2.jpg',
  //   graphUrl: 
  //       'https://www.foreximf.com/upload/image/content/line-chart.png',
  //   duration: 10,
  //   ingredients: [
  //     'Current profit: (+) Rp10.000.000',
  //     'Good Investment 👍'
  //   ],
  //   isProfit: true,
  //   isRugi: false,
  // ),
  // Meal(
  //   id: 'm3',
  //   categories: [
  //     'c3',
  //   ],
  //   title: 'MyHamburgers',
  //   affordability: Affordability.LowBudgetFriendly,
  //   complexity: Complexity.Simple,
  //   imageUrl:
  //       'https://mir-s3-cdn-cf.behance.net/project_modules/disp/74494a8887791.560e69eda2f70.JPG',
  //   graphUrl: 
  //       'https://s3-ap-southeast-1.amazonaws.com/ekrutassets/home/deploy/ekrut/releases/20210318090253/public/ckeditor_assets/pictures/6453/content_Line_Chart.png',
  //   duration: 45,
  //   ingredients: [
  //     'Current profit: (+) Rp20.000',
  //     'Good Investment 👍'
  //   ],
  //   isProfit: true,
  //   isRugi: false,
  // ),
  Meal(
    id: 'm4',
    categories: [
      'c4',
    ],
    title: 'Ternak Babi Ngepet',
    affordability: Affordability.HighRisk,
    complexity: Complexity.Challenging,
    imageUrl:
        'https://i.ytimg.com/vi/FnCwD4SDpjU/maxresdefault.jpg',
    deskripsi: 'Raih keuntungan dengan cara ngepet',
    duration: 60,
    ingredients: 
      'Jumlah lembar saham yang telah Anda tanamkan:\n20 lembar'
    ,
    hargaSaham: 2000000,
    jumlahLembar: 20202,
    sisaLembar: 202,
    isProfit: false,
    isRugi: true,
  ),
  Meal(
    id: 'm5',
    categories: [
      'c5',
    ],
    title: 'Pebepe\'s Shop',
    affordability: Affordability.HighRisk,
    complexity: Complexity.Simple,
    imageUrl:
        'https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5fc5545f851237f27b13a0db%2F0x0.jpg%3FcropX1%3D0%26cropX2%3D7952%26cropY1%3D630%26cropY2%3D5106',
    deskripsi: 'Ingin belajar web dan mobile dalam satu semester? Pebepe adalah solusinya',
    // graphUrl: 
    //     'https://d138zd1ktt9iqe.cloudfront.net/media/seo_landing_files/reading-a-line-graph-1-1624252264.png',
    duration: 15,
    ingredients: 
      'Jumlah lembar saham yang telah Anda tanamkan:\n300 lembar'
    ,
    hargaSaham: 10000,
    jumlahLembar: 202,
    sisaLembar: 10,
    isProfit: true,
    isRugi: false,
  ),
  Meal(
    id: 'm6',
    categories: [
      'c6',
    ],
    title: 'BizzVest',
    affordability: Affordability.MiddleHigh,
    complexity: Complexity.Hard,
    imageUrl:
        'https://i.imgur.com/exltf82.png',
    // graphUrl: 
    //     'https://camo.githubusercontent.com/22c2fafa60804cb2bc3f60ff8afe6d5262da3b25/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f7a654d6972636f2f6769746875622f73776966742d6c696e6563686172742f30312e706e67',
    deskripsi: 'Let’s invest toward your business',
    duration: 240,
    ingredients: 
      'Jumlah lembar saham yang telah Anda tanamkan:\n200 lembar'
    ,
    hargaSaham: 200000,
    jumlahLembar: 2002,
    sisaLembar: 202,
    isProfit: true,
    isRugi: false,
  ),
];
